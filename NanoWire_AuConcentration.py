from math import sqrt
import matplotlib.pyplot as plt

R = 200  # outer radius (radius of nanowire)
r = 100  # inner radius (radius of core)
core_concentration = 0.5
shell_concentration = 0.6

pe = 1.1  # plot margins => 1.1 is 110 %

x_range_in = range(int(r))
x_range_out = range(int(r), int(R) + 1)
x_range = range(int(R) + 1)

x = x_range

plot_x_min = R * (1 - pe)
plot_x_max = R * pe
plot_y_min = R * (1 - pe)
plot_y_max = R * pe

R = float(R)
r = float(r)

# SHELL CALCULATION
y1 = []
for i in x_range_in:
    # y_i = R * cos(arcsin(x[i] / R)) - r * cos(arcsin(x[i] / r))  # old calculation
    y_i = (sqrt(R ** 2 - x[i] ** 2) - sqrt(r ** 2 - x[i] ** 2)) * shell_concentration
    y1.append(y_i)
for i in x_range_out:
    y_i = sqrt(R ** 2 - x[i] ** 2) * shell_concentration
    y1.append(y_i)
plt.subplot(221, aspect='equal')
plt.title('Shell projection')
plt.axis([plot_x_min, plot_x_max, plot_y_min, plot_y_max])
plt.xlabel("Radius [nm]")
plt.ylabel("Effective thickness [nm]")
plt.plot(x, y1)

# CORE CALCULATION
y2 = []
for i in x_range_in:
    y_i = sqrt(r ** 2 - x[i] ** 2) * core_concentration
    y2.append(y_i)
for i in x_range_out:
    y2.append(0)
plt.subplot(222, aspect='equal')
plt.title('Core projection')
plt.axis([plot_x_min, plot_x_max, plot_y_min, plot_y_max])
plt.xlabel("Radius [nm]")
plt.ylabel("Effective thickness [nm]")
plt.plot(x, y2)

# SUM OF SHELL AND CORE
y3 = []
for i in x_range:
    y_i = y1[i] + y2[i]
    y3.append(y_i)
plt.subplot(223, aspect='equal')
plt.title('Shell + core projection')
plt.axis([plot_x_min, plot_x_max, plot_y_min, plot_y_max])
plt.xlabel("Radius [nm]")
plt.ylabel("Effective thickness [nm]")
plt.plot(x, y3)

# CONCENTRATION
y4 = []
for i in x_range:
    try:
        y_i = (y3[i] / sqrt(R ** 2 - x[i] ** 2)) * 100
    except ZeroDivisionError:  # this happens for last value where the effective thickness goes to
        # zero causing zero division error.
        y_i = shell_concentration * 100 if r < R else core_concentration
    y4.append(y_i)
plt.subplot(224, aspect='equal')
plt.title('Concentration')
plt.axis([plot_x_min, plot_x_max, -10, 110])
plt.xlabel("Radius [nm]")
plt.ylabel("Concentration [%]")
plt.plot(x, y4)

# adjust and show all plots
plt.subplots_adjust(top=0.9, bottom=0.1, left=0.1, right=0.9, hspace=0.4, wspace=0.4)
plt.show()
